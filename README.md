-- Rest service 

Here are endpoints for services. Add and update services accepts json input. Sample usage is documented in Person_postman.docx in PersonApp project. 
http://localhost:8085/person/addPerson
http://localhost:8085/Person/api/getPerson/{firstName}/{lastName}
http://localhost:8085/person/updatePerson

-- Database

MySql latest available version 8.0.17 in docker is used. 
docker-compose.yml location :- /PersonBootApi/src/main/resources/docker-compose.yml
Dump files required to run application :- /PersonBootApi/src/main/resources/MySqlDump

-- Security/Authentication

Rest endpoint updatePerson is secured with http basic authentication and only user with role “ADMIN” has access.
Admin credentials:- admin/admin
Actuator is secured with http basic authentication, only users with role “METRICS” 
Actuator credentials:- rajesh/rajesh

Users and authorizations are managed using database tables USERS and AUTHORITIES. 

-- Resilience

A circuit switch is implemented between rest end point getPerson and service layer. Switch is configured to OPEN on threshold of 20% failures for total of 10 requests.

-- Actuator

Endpoints auditevents,health, metrics, info are exposed.
Customized health endpoint, to display status of switch.
 
 
